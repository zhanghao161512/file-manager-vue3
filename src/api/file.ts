import {FileEntity} from "zhangyida-tools";
import {deepCopy, shallowCopy, shallowCopyIgnore} from "../util/common";

const {FileEntity: File, ListProcess} = require('zhangyida-tools');

export class FileTagEntity extends File {
    tag: string[] = []
    selected?: boolean = false
    desc?: string
    isOnline?: boolean;

    static ofJson(json: any) {
        const fileTagEntity = new FileTagEntity();
        shallowCopyIgnore(json, fileTagEntity)
        return fileTagEntity;
    }
}


function getDataJsonEntityWithDefault(path: string, jsonFileName: string, fileContent = "[]") {
    return File.ofNullable(path, jsonFileName).orElse(() => {
        return File.of(path).createFile(jsonFileName, fileContent);
    });
}

const USER_HOME = process.env.HOME || process.env.USERPROFILE
export const TAG_DATA_FILE_NAME = "tagData.json";
const TAG_FILE_NAME = 'tag.json';
const CONFIG_DIR_NAME = "file_tag_config";
export const CONFIG_DIR = File.pathJoin(USER_HOME, CONFIG_DIR_NAME);
File.ofNullable(CONFIG_DIR).orElse(() => File.of(File.getParentFolderPathByPath(CONFIG_DIR)).createChildFolder(CONFIG_DIR_NAME))
export const DATA_JSON_ENTITY = getDataJsonEntityWithDefault(CONFIG_DIR, TAG_DATA_FILE_NAME);
export const TAG_DATA_ENTITY = getDataJsonEntityWithDefault(CONFIG_DIR, TAG_FILE_NAME);

export const CUSTOM_FORM_JSON = getDataJsonEntityWithDefault(CONFIG_DIR, "custom_form.json", "{}").json()

const searchFormTags = [
    "el-input",
    "el-select",
    "el-cascader",
    "el-radio-group",
    "el-checkbox-group",
    "el-switch",
    "el-rate"
]

export const search_field_process = new Map<string, string>();

function getCustomSearchFormJson() {
    const filterSearchFormJson = (fields: any = []) => {
        return fields.filter((v: any) => {
            if (v.__config__.children) {
                v.__config__.children = filterSearchFormJson(v.__config__.children);
            }
            const b = searchFormTags.includes(v.__config__?.tag) || v.__config__.children;
            if (b && v.__vModel__) {
                const isText = v.__config__?.tag === "el-input";
                search_field_process.set(v.__vModel__, isText ? 'text' : 'equal')
            }
            return b;
        })
    };
    let copyJson = deepCopy(CUSTOM_FORM_JSON);

    copyJson = {
        ...copyJson,
        fields: filterSearchFormJson(copyJson.fields)
    }
    return copyJson;
}

function setFields(json: any[] = [], fields: CustomField[]) {
    json.forEach((v: any) => {
        if (v?.__config__?.children) {
            setFields(v?.__config__?.children, fields);
        } else {
            fields.push({property: v.__vModel__, label: v.__config__.label})
        }
    });
}

type CustomField = { property: string, label: string };
export const customFields: CustomField[] = []
setFields(CUSTOM_FORM_JSON.fields, customFields);

export const CUSTOM_SEARCH_FORM_JSON = getCustomSearchFormJson();

export function fileEqual(file1: any, file2: any) {
    if (file1.isDisk() && file2.isDisk()) {
        return file1.filePath == file2.filePath
    } else {
        return file1.ino === file2.ino
    }
}

export function filesContains(files: any[], file: any) {
    return files.some(value => {
        return fileEqual(value, file)
    })
}

function getLevel(path: string) {
    return path.split("/").filter(Boolean).length;
}

export const allFiles = ref<FileTagEntity[]>([]);

function getDbData() {
    // object.forEach(value => {
    //     // @ts-ignore
    //     value.tag = value.tag.filter(Boolean)
    // })
    if (allFiles.value.length === 0) {
        allFiles.value = sortFile(DATA_JSON_ENTITY.json().map((v: FileTagEntity) => FileTagEntity.ofJson(v))) || [];
    }
}

export function getDbTag() {
    const object = TAG_DATA_ENTITY.json() || [];
    return ListProcess.of([...object]).unique().toList()
}

// export function removeStartWithFile(value: FileTagEntity) {
//     allFiles.value = allFiles.value.filter(value1 => !value1.filePath.startsWith(value.filePath))
// }

function syncDbData(actual: FileTagEntity[], path: string) {
    getDbData();
    // const currentLevel = getLevel(path);
    // const isCurrentChildFiles = (value: FileTagEntity) => value.filePath.startsWith(path) && getLevel(value.filePath) === currentLevel + 1;
    actual.forEach(value => {
        const disk = value.isDisk();
        if (disk) {
            value.isOnline = true;
        }
        const dbData = allFiles.value.find((exist: any) => {
            return fileEqual(exist, value);
        });
        if (!dbData) {
            value.tag = []
            allFiles.value.push(value)
        } else {
            syncInfo(dbData, value);
        }
    })
    writeToDataFile()
}

export type FileEntityProcess = (file: any) => void;
export const updateFileTagEntities = (files: FileTagEntity[], process: FileEntityProcess) => {
    const filter = allFiles.value.filter(value => filesContains(files, value));
    for (let file of filter) {
        process(file)
    }
    writeToDataFile()
}

const TypeSort = [
    'disk', 'folder', 'file'
]

export function sortFile(actual: FileTagEntity[]) {
    // @ts-ignore
    return actual.sort((a, b) => {
        if (a.type !== b.type) {
            return TypeSort.indexOf(a.type) - TypeSort.indexOf(b.type)
        } else if (a.type === b.type && a.type === 'file') {
            if (a.getNameAndExt()[1] > b.getNameAndExt()[1]) {
                return 1
            } else if (a.getNameAndExt()[1] < b.getNameAndExt()[1]) {
                return -1
            } else {
                if (a.getNameAndExt()[0] > b.getNameAndExt()[0]) {
                    return 1
                } else if (a.getNameAndExt()[0] < b.getNameAndExt()[0]) {
                    return -1
                } else {
                    return 0;
                }
            }
        }
    });
}

export const fetchWithDisk = async (path = "", isFolder?: boolean) => {
    let actual = await File.fetchWithDisk(path, isFolder);
    syncDbData(actual, path);
    actual = sortFile(actual)
    return actual
}

export const OPERATION = {
    MOVE: 'move',
    RENAME: 'rename',
    COPY: 'copy',
}

function copy(element: any) {
    return JSON.parse(JSON.stringify(element));
}

export function syncInfo(dbData: any, actualData: FileTagEntity) {
    shallowCopyIgnore(dbData, actualData, [
        "fileName",
        "filePath",
        "type",
        "size",
        "lastUpdateTime",
        "createTime",
        "ino",
        "isOnline"
    ])
    shallowCopy(actualData, dbData, [
        'fileName', 'filePath', 'type', 'isOnline', 'createTime',
        'lastUpdateTime',
        'size'
    ])
}

export function writeToDataFile() {
    const data = allFiles.value.map(value => {
        if (!value.ino) {
            throw new Error('ino 不能为空')
        }
        const obj = {};
        Object.keys(value).forEach(key => {
            // 不需要存的字段
            if (!['_removed',
                'selected'
            ].includes(key)) {
                // @ts-ignore
                obj[key] = value[key]
            }
        })
        return obj;
    });
    if (allFiles.value.length !== 0) {
        DATA_JSON_ENTITY.writeJson(data)
    }
}

export function syncDataJson(oldFilePath: string, newFileEntity = new File(), operation = OPERATION.RENAME) {
    const oldPathLevel = getLevel(oldFilePath);
    const childFilesPredicate = (value: FileTagEntity) => value.filePath.startsWith(oldFilePath) && getLevel(value.filePath) > oldPathLevel;
    const dataJson = allFiles.value;
    const findIndex = dataJson.findIndex(value => value.filePath === oldFilePath);

    const replaceOldPath = (value: FileTagEntity) => {
        value.filePath = value.filePath.replace(oldFilePath, newFileEntity.filePath)
    };

    const moveAndRenameFunc = () => {
        if (findIndex !== -1) {
            dataJson[findIndex] = newFileEntity;
            if (newFileEntity.isDirectory()) {
                dataJson.filter(childFilesPredicate).forEach(value => {
                    replaceOldPath(value);
                })
            }
        }
    };
    const map = {
        [OPERATION.RENAME]: moveAndRenameFunc,
        [OPERATION.COPY]: () => {
            if (findIndex !== -1) {
                const copyJson = copy(dataJson[findIndex]);
                copyJson.filePath = newFileEntity.filePath
                copyJson.fileName = newFileEntity.fileName
                dataJson.push(FileTagEntity.ofJson(copyJson))
                if (newFileEntity.isDirectory()) {
                    let copyRelatedJson = dataJson.filter(childFilesPredicate);
                    copyRelatedJson = copy(copyRelatedJson);
                    copyRelatedJson.forEach(value => {
                        replaceOldPath(value)
                    })
                    dataJson.push(...copyRelatedJson)
                }
            }
        },
        [OPERATION.MOVE]: moveAndRenameFunc,
    }

    map[operation]()

    writeToDataFile();
}

function scheduleSaveAndGetDbData() {
    /*第一次获取数据*/
    getDbData()
    setInterval(() => {
        writeToDataFile()
        getDbData()
        console.log("定时保存成功")
    }, 30 * 1000)
}

scheduleSaveAndGetDbData();
